﻿using LanguageExt;

namespace DocSearchAIO.DocSearch.TOs;

public sealed record DownloadFileResponse(FileStream DownloadFileStream, string ReturnFileName, string ContentType);