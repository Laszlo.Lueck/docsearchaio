using System.Text.Json.Serialization;
using LanguageExt;

namespace DocSearchAIO.DocSearch.TOs;

public sealed record ContentDetail(
    [property: JsonPropertyName("contentText")] string ContentText
);