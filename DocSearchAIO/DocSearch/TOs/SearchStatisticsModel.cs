﻿using System.Text.Json.Serialization;
using LanguageExt;

namespace DocSearchAIO.DocSearch.TOs;

public sealed record SearchStatisticsModel(
    [property: JsonPropertyName("searchTime")] long SearchTime,
    [property: JsonPropertyName("docCount")] long DocCount
);