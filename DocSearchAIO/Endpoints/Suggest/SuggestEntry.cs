using LanguageExt;

namespace DocSearchAIO.Endpoints.Suggest;

public sealed record SuggestEntry(string Label, string[] IndexNames);