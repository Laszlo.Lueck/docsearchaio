using LanguageExt;

namespace DocSearchAIO.Endpoints.Suggest;

public sealed record SuggestResult(string SearchPhrase, IEnumerable<SuggestEntry> Suggests);