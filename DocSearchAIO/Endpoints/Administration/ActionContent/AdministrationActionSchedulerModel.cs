using LanguageExt;

namespace DocSearchAIO.Endpoints.Administration.ActionContent;

public sealed record AdministrationActionSchedulerModel(string SchedulerName,
    IEnumerable<AdministrationActionTriggerModel> Triggers);